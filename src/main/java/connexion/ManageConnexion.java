package connexion;

import org.hibernate.*;
import pojo.Professeur;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class ManageConnexion {
    Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
    SessionFactory sessionFactory = cfg.buildSessionFactory();

    public List<Professeur> professeurList() {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List prof = new ArrayList();


        try {
            tx = session.beginTransaction();
            String sql = "SELECT * FROM prof";
            SQLQuery query = session.createSQLQuery(sql);
            query.addEntity(Professeur.class);
            prof = query.list();
            return prof;
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

        return prof;
    }
}
