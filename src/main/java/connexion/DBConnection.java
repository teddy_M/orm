package connexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private static String url = "jdbc:mysql://localhost:3306/scolaire?serverTimezone=UTC";
    private static String user = "root";
    private static String password = "root";
    private static Connection connect;

    public static Connection getInstance() {
        if (connect == null) {
            try{
                connect = DriverManager.getConnection(url, user, password);
                System.out.println("Instanciation de la connexion SQL");

            }catch (SQLException e) {
                e.printStackTrace();
            }
        }else{
            System.out.println("NOPE SQL") ;
        }
        return connect ;
    }
}
