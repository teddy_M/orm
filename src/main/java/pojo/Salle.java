package pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Salle implements Serializable{

    @Id
    @GeneratedValue
    private int idSalle;

    private String nomSalle;

    public Salle() {
    }

    public Salle(int idSalle, String nomSalle) {
        this.idSalle = idSalle;
        this.nomSalle = nomSalle;
    }

    public int getIdSalle() {
        return idSalle;
    }

    public void setIdSalle(int idSalle) {
        this.idSalle = idSalle;
    }

    public String getNomSalle() {
        return nomSalle;
    }

    public void setNomSalle(String nomSalle) {
        this.nomSalle = nomSalle;
    }
}
