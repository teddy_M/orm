package pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Professeur implements Serializable {

    @Id
    @GeneratedValue
    private int idProf;

    private String nomProf;
    private String login;
    private String password;
    private int admin;

    public Professeur() {

    }

    public Professeur(int idProf, String nomProf, String login, String password, int admin) {
        this.idProf = idProf;
        this.nomProf = nomProf;
        this.login = login;
        this.password = password;
        this.admin = admin;
    }

    public int getIdProf() {
        return idProf;
    }

    public void setIdProf(int idProf) {
        this.idProf = idProf;
    }

    public String getNomProf() {
        return nomProf;
    }

    public void setNomProf(String nomProf) {
        this.nomProf = nomProf;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAdmin() {
        return admin;
    }

    public void setAdmin(int admin) {
        this.admin = admin;
    }
}
